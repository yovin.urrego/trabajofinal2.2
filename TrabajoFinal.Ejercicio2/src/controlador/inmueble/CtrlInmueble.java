/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.inmueble;

import controlador.utiles.TipoBienInmueble;
import modelo.binmuebles.BienInmueble;

/**
 *
 * @author Yovin
 */
public class CtrlInmueble extends BienInmueble {

    public CtrlInmueble(String ubicacion, String propietario, TipoBienInmueble tipoInmueble, Double costo) {
        super(ubicacion, propietario, tipoInmueble, costo);
    }
    
}
