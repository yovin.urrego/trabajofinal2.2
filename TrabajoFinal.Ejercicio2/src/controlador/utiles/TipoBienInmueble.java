/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package controlador.utiles;

/**
 *
 * @author Yovin
 */
public enum TipoBienInmueble {
    NATURALEZA, INCORPORACION;
}
